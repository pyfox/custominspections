# CustomInspections

A plugin for IntellIJ products for writing custom code inspections using regular expressions.

![Screenshot](/data/settings.png)

## Configuration options

### Path pattern

Pattern to match the file path of the files to be inspected. Here are some examples:

* `.*\.txt` - Match any file ending with ".txt" in any directory of the project.
* `src/.*` - Match any file within the "src" directory.
* `out/Changelog\.xml` - Match the file "Changelog.xml" contained in the "data" directory.

### Content pattern

Pattern that the user should be blamed for. It is recommended to try this pattern
in the editor first, using the Ctrl+F search with active "regex" evaluation.

Multiline mode is enabled. So ^ matches start of line, $ matches end of line.


### Replacement pattern

The pattern to be used for Quickfixes (Will be displayed in the context actions using the Lightbulb in your code 
editor).

You can refer to groups from the "Content Pattern". For example the following combination will replace 
`123 numbers` with `123 digits`

* Content pattern `(\d+) numbers`
* Replacement pattern `$1 digits`

### Description

The description to show in the tooltip when the error is hovered.

### Severity

The severity to display.