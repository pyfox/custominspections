package com.pyfips.intellij.custominspections;

import com.intellij.codeInspection.InspectionManager;
import com.intellij.codeInspection.LocalInspectionTool;
import com.intellij.codeInspection.LocalQuickFix;
import com.intellij.codeInspection.ProblemDescriptor;
import com.intellij.openapi.diagnostic.Logger;
import com.intellij.openapi.editor.Document;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.util.TextRange;
import com.intellij.psi.PsiDocumentManager;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiFile;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.nio.file.InvalidPathException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.regex.MatchResult;
import java.util.regex.Matcher;


/**
 * The Inspection
 */
public class CustomInspectionsInspector extends LocalInspectionTool {
    private final static Logger LOG = Logger.getInstance(CustomInspectionsInspector.class);

    @Nullable
    @Override
    public ProblemDescriptor[] checkFile(@NotNull PsiFile file, @NotNull InspectionManager manager, boolean isOnTheFly) {

        // long startTime = System.currentTimeMillis();
        List<ProblemDescriptor> problems = new ArrayList<>();
        String path = file.getVirtualFile().getCanonicalPath();
        String projectBasePath = manager.getProject().getBasePath();
        Path filePath;

        if (path == null) {
            return null;
        }
        if (projectBasePath == null) {
            projectBasePath = "";
        }  // e.g. when showing diff in VCS.

        try {
            filePath = Paths.get(path);
        } catch (InvalidPathException ignored) {
            // E.g. "/PythonRegExp Fragment (File.py:311).regexp" when using "Check RegExp" intention
            LOG.warn("Did not inspect file \"" + path + "\": This does not seem to be a valid path.");
            return null;
        }

        try {
            // Maybe the project path does not exist (when using standalone editor), or the file
            // is not part of the project
            Path projectPath = Paths.get(projectBasePath);
            filePath = projectPath.relativize(filePath);
        } catch (IllegalArgumentException ignored) {}

        CustomInspectionsConfig config = manager.getProject().getService(CustomInspectionsConfig.class);

        List<ElementData> dataSet = config.getPatterns(filePath.toString().replace('\\', '/'));
        for (ElementData data : dataSet) {

            Matcher matcher = data.getCompiledContentPattern().matcher(file.getText());
            while (matcher.find()) {
                MatchResult match_result = matcher.toMatchResult();
                LocalQuickFix quickfix = null;
                if (!Objects.equals(data.getReplacementPattern(), "")) {
                    quickfix = new CustomInspectionsQuickFix(matcher, match_result.start(),
                            data.getReplacementPattern(),
                            data.getDescription());
                }
                ProblemDescriptor problemDescriptor = manager.createProblemDescriptor(file,
                        TextRange.create(match_result.start(), match_result.end()), data.getDescription(),
                        data.getSeverityHighlightType(), true, quickfix);
                problems.add(problemDescriptor);
            }
        }

        // Performance logging
        //LOG.warn(String.valueOf(System.currentTimeMillis() - startTime));
        return problems.toArray(new ProblemDescriptor[0]);
    }

    private static class CustomInspectionsQuickFix implements LocalQuickFix {
        private Matcher regexMatcher;
        private int startPosition;
        private String quickfixPattern;
        private String description;

        public CustomInspectionsQuickFix(Matcher regexMatcher, int startPosition, String replacementPattern, String description) {
            this.regexMatcher = regexMatcher;
            this.startPosition = startPosition;
            this.quickfixPattern = replacementPattern;
            this.description = description;
        }

        @Override
        public @NotNull String getFamilyName() {
            String description = this.description;
            if (description.length() > 30) {
                description = description.substring(0, 30) + "...";
            }
            return "Fix \"" + description + "\"";
        }

        @Override
        public void applyFix(@NotNull Project project, @NotNull ProblemDescriptor descriptor) {
            PsiElement element = descriptor.getPsiElement();
            @Nullable Document document = PsiDocumentManager.getInstance(project).getDocument(element.getContainingFile());
            if (document == null) {
                return;
            }
            StringBuilder buffer = new StringBuilder();
            this.regexMatcher.reset();
            while (this.regexMatcher.find()) {
                // Apply the regex again to find the corresponding match.
                // .find(this.startPosition) cannot be used because differing start position for negative lookbehinds.
                if (this.regexMatcher.start() == this.startPosition) {
                    this.regexMatcher.appendReplacement(buffer, this.quickfixPattern);
                    break;
                }
            }
            this.regexMatcher.appendTail(buffer);
            document.setText(buffer.toString());
        }
    }
}
