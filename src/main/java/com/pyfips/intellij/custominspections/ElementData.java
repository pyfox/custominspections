package com.pyfips.intellij.custominspections;

import com.intellij.codeInspection.ProblemHighlightType;

import java.util.regex.Pattern;

/**
 * One regex entry
 */
public final class ElementData {
    private String pathPattern;
    private String contentPattern;
    private String replacementPattern;
    private String description;
    private ProblemHighlightType severity;
    private Pattern compiledPattern;

    public ElementData(String aPathPattern, String aContentPattern, String aReplacementPattern, String aDescription, String aSeverity) {
        pathPattern = aPathPattern;
        contentPattern = aContentPattern;
        replacementPattern = aReplacementPattern;
        description = aDescription;
        severity = this.SeverityStr2PHT(aSeverity);
        compilePattern();
    }

    @SuppressWarnings("unused")  // Used when creating a new item using the "new" toolbar button.
    public ElementData() {
        pathPattern = "";
        contentPattern = "";
        replacementPattern = "";
        description = "";
        severity = ProblemHighlightType.WARNING;
    }

    public String getPathPattern() {
        return pathPattern;
    }

    public void setPathPattern(String newPattern) {
        pathPattern = newPattern;
    }

    private void compilePattern() {
        compiledPattern = Pattern.compile(contentPattern, Pattern.MULTILINE);
    }

    public Pattern getCompiledContentPattern() {
        if (compiledPattern == null) {
            compilePattern();
        }
        return compiledPattern;
    }

    public String getContentPattern() {
        return contentPattern;
    }

    public void setContentPattern(String newPattern) {
        contentPattern = newPattern;

        // Will be set on keypress while typing. Do not compile here because
        // there will always be invalid patterns while typing.
        compiledPattern = null;
    }

    public String getReplacementPattern() { return replacementPattern; }

    public void setReplacementPattern(String newPattern) {
        replacementPattern = newPattern;

        // Will be set on keypress while typing. Do not compile here because
        // there will always be invalid patterns while typing.
        compiledPattern = null;
    }

    public String getDescription() {
        return description;
    }

    public String getSeverity() {
        return this.SeverityPHT2Str(severity);
    }

    public ProblemHighlightType getSeverityHighlightType() {
        return severity;
    }

    public void setSeverity(String newSeverity) {
        severity = this.SeverityStr2PHT(newSeverity);
    }

    public void setDescription(String newDescription) {
        description = newDescription;
    }

    private ProblemHighlightType SeverityStr2PHT(String s) {
        if (s.equals("Error")) {
            return ProblemHighlightType.ERROR;
        }
        if (s.equals("Weak Warning")) {
            return ProblemHighlightType.WEAK_WARNING;
        }
        if (s.equals("Info")) {
            return ProblemHighlightType.INFORMATION;
        }
        return ProblemHighlightType.WARNING;
    }

    private String SeverityPHT2Str(ProblemHighlightType pht) {
        if (pht == ProblemHighlightType.ERROR) {
            return "Error";
        }
        if (pht == ProblemHighlightType.WEAK_WARNING) {
            return "Weak Warning";
        }
        if (pht == ProblemHighlightType.INFORMATION) {
            return "Info";
        }
        return "Warning";
    }
}