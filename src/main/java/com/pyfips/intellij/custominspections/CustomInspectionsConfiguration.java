package com.pyfips.intellij.custominspections;

import com.intellij.openapi.options.Configurable;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.ComboBox;
import com.intellij.util.Function;
import com.intellij.util.ui.ColumnInfo;
import com.intellij.util.ui.ListTableModel;
import com.intellij.util.ui.table.TableModelEditor;
import com.intellij.util.ui.table.TableModelEditor.EditableColumnInfo;
import org.jetbrains.annotations.Nls;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;
import javax.swing.table.TableCellEditor;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.*;
import java.util.List;


/**
 * Settings GUI.
 */
public class CustomInspectionsConfiguration implements Configurable {

    private TableModelEditor<ElementData> editor;
    private final Project project;

    public CustomInspectionsConfiguration(Project project) {
        this.project = project;
    }

    @Override
    public String getDisplayName() {
        return "Custom Inspections Plugin";
    }

    @Override
    public JComponent createComponent() {
        CustomInspectionsConfig config = project.getService(CustomInspectionsConfig.class);
        CustomInspectionsConfig.InternalState state = config.myState;
        return createRootPanel(state);
    }

    private JComponent createRootPanel(CustomInspectionsConfig.InternalState state) {
        @SuppressWarnings("rawtypes") ColumnInfo[] COLS = {
                new EditableColumnInfo<ElementData, String>() {
                    @Override

                    public String getName() {
                        return "Path Pattern (?)";
                    }

                    @Nullable
                    @Override
                    public String valueOf(ElementData o) {
                        return o.getPathPattern();
                    }

                    @Override
                    public void setValue(ElementData aListElement, String value) {
                        aListElement.setPathPattern(value);
                    }

                    @Override
                    public TableCellEditor getEditor(ElementData elementData) {
                        return new TextCellEditor(elementData.getPathPattern(), elementData, this);
                    }

                    @Nls(capitalization = Nls.Capitalization.Sentence)
                    @Override
                    public String getTooltipText() {
                        return "Path pattern as regular expression. Path is is relative to the project directory and divided by slashes (/).<br/><br/><u>Examples</u><ul><li>Check any txt file: <b>.*\\.txt</b></li><li>Check every file in src directory: <b>src/.*</b></li></ul>";
                    }
                },
                new EditableColumnInfo<ElementData, String>() {
                    @Override
                    public String getName() {
                        return "Content Pattern (?)";
                    }

                    @Nullable
                    @Override
                    public String valueOf(ElementData o) {
                        return o.getContentPattern();
                    }

                    @Override
                    public void setValue(ElementData aListElement, String value) {
                        aListElement.setContentPattern(value);
                    }

                    @Override
                    public TableCellEditor getEditor(ElementData elementData) {
                        return new TextCellEditor(elementData.getContentPattern(), elementData, this);
                    }

                    @Nls(capitalization = Nls.Capitalization.Sentence)
                    @Override
                    public String getTooltipText() {
                        return "Bad pattern.<br />If this pattern is found, it will be highlighted as warning, error or whatever is selected in 'Severity'.<br>Multiline mode is enabled, so use ^ for line start and $ for line end.<br /><br /><u>Examples</u></br><ul><li>To match the characters \"hello\" type: <b>hello</b></li><li>Match python classes with lower case name: <b>class [a-z][a-zA-Z0-9]*(:|\\()</b></li></ul>";
                    }
                },
                new EditableColumnInfo<ElementData, String>() {
                    @Override
                    public String getName() {
                        return "Replacement Pattern (?)";
                    }

                    @Nullable
                    @Override
                    public String valueOf(ElementData o) {
                        return o.getReplacementPattern();
                    }

                    @Override
                    public void setValue(ElementData aListElement, String value) {
                        aListElement.setReplacementPattern(value);
                    }

                    @Override
                    public TableCellEditor getEditor(ElementData elementData) {
                        return new TextCellEditor(elementData.getReplacementPattern(), elementData, this);
                    }

                    @Nls(capitalization = Nls.Capitalization.Sentence)
                    @Override
                    public String getTooltipText() {
                        return "Replacement to be used for 'QuickFix' actions.<br />You can create groups in 'Content Pattern' column using braces and use them as replacement values here.<br /><br /><u>Examples</u><br /><br /><b>Content Pattern: </b>(\\d+) numbers<br /><b>Replacement Pattern: </b>$1 digits<br/>...will replace '123 numbers' with '123 digits'.";
                    }
                },
                new EditableColumnInfo<ElementData, String>() {
                    @Override
                    public String getName() {
                        return "Description (?)";
                    }

                    @Nullable
                    @Override
                    public String valueOf(ElementData o) {
                        return o.getDescription();
                    }

                    @Override
                    public void setValue(ElementData aListElement, String value) {
                        aListElement.setDescription(value);
                    }

                    @Override
                    public TableCellEditor getEditor(ElementData elementData) {
                        return new TextCellEditor(elementData.getDescription(), elementData, this);
                    }

                    @Nls(capitalization = Nls.Capitalization.Sentence)
                    @Override
                    public String getTooltipText() {
                        return "The description will be shown when hovering the error.";
                    }
                },
                new EditableColumnInfo<ElementData, String>() {
                    @Override
                    public String getName() {
                        return "Severity (?)";
                    }

                    @Override
                    public TableCellEditor getEditor(ElementData elementData) {
                        return new SeverityCellEditor();
                    }

                    @Override
                    public String valueOf(ElementData o) {
                        return o.getSeverity();
                    }

                    @Override
                    public void setValue(ElementData aListElement, String value) {
                        aListElement.setSeverity(value);
                    }

                    @Nls(capitalization = Nls.Capitalization.Sentence)
                    @Override
                    public String getTooltipText() {
                        return "Inspection severity.";
                    }
                }
        };

        TableModelEditor.DialogItemEditor<ElementData> cellEditor = new TableModelEditor.DialogItemEditor<>() {

            @NotNull
            @Override
            public Class<? extends ElementData> getItemClass() {
                return ElementData.class;
            }

            @Override
            public ElementData clone(@NotNull ElementData aListElement, boolean b) {
                return new ElementData(
                        aListElement.getPathPattern(),
                        aListElement.getContentPattern(),
                        aListElement.getReplacementPattern(),
                        aListElement.getDescription(),
                        aListElement.getSeverity());
            }

            @Override
            public void edit(@NotNull ElementData elementData, @NotNull Function<? super ElementData, ? extends ElementData> function, boolean b) {
            }

            @Override
            public void applyEdited(@NotNull ElementData aListElement, @NotNull ElementData t1) {
            }

            @Override
            public boolean isEditable(@NotNull ElementData item) {
                return false;
            }
        };

        editor = new TableModelEditor<>(COLS, cellEditor, "");
        UpdateModel(editor.getModel(), state);
        return editor.createComponent();
    }

    private void UpdateModel(ListTableModel<ElementData> model, CustomInspectionsConfig.InternalState state) {
        for (Map.Entry<String, List<ElementData>> dictEntry : state.state.entrySet()) {
            for (ElementData listEntry : dictEntry.getValue()) {
                model.addRow(listEntry);
            }
        }
    }

    @Override
    public boolean isModified() {
        return true;
    }

    @Override
    public void apply() {
        CustomInspectionsConfig config = project.getService(CustomInspectionsConfig.class);

        config.clearState();

        ListTableModel<ElementData> mod = editor.getModel();

        for (int i = 0; i < mod.getRowCount(); i++) {
            config.setValue(mod.getValueAt(i, 0).toString(),
                    mod.getValueAt(i, 1).toString(),
                    mod.getValueAt(i, 2).toString(),
                    mod.getValueAt(i, 3).toString(),
                    mod.getValueAt(i, 4).toString());
        }
        config.updateCompiledState();
    }
}

/**
 * Cell editor for severity. Basically a dropdown menu.
 */
class SeverityCellEditor extends AbstractCellEditor implements TableCellEditor, ActionListener {
    private String value;

    @Override
    public Component getTableCellEditorComponent(JTable jTable, Object o, boolean b, int i, int i1) {
        String currentChoice = "Warning";
        if (o instanceof ElementData) {
            ElementData elementData = (ElementData) o;
            currentChoice = elementData.getSeverity();
        }
        value = currentChoice;
        JComboBox<String> cb = new ComboBox<>();
        cb.addItem("Error");
        cb.addItem("Warning");
        cb.addItem("Weak Warning");
        cb.addItem("Info");
        cb.setSelectedItem(currentChoice);
        cb.addActionListener(this);

        return cb;
    }

    @Override
    public String getCellEditorValue() {
        return value;
    }

    @Override
    public void actionPerformed(ActionEvent actionEvent) {
        @SuppressWarnings("unchecked") JComboBox<ElementData> cb = (ComboBox<ElementData>) actionEvent.getSource();
        value = (String) cb.getSelectedItem();
    }
}

/**
 * Simple text cell editor.
 * The main reason this exists is to explicitly call <EditableColumnInfo>.SetValue as soon as the text value has changed.
 * Otherwise, <EditableColumnInfo>.SetValue would only be called if the cell focus is lost.
 * With the default text editor, if you are editing a cell and hit the "OK" button of the settings dialog,
 * .SetValue would be called *after* the dialog has been saved, thus the changes would not be applied.
 */
@SuppressWarnings("rawtypes")
class TextCellEditor extends AbstractCellEditor implements TableCellEditor, KeyListener {
    private final String value;
    private JTextField textField = null;
    private final ElementData elementData;
    private final EditableColumnInfo colInfo;

    public TextCellEditor(String value, ElementData elementData, EditableColumnInfo colInfo) {
        this.value = value;
        this.elementData = elementData;
        this.colInfo = colInfo;
    }

    @Override
    public Component getTableCellEditorComponent(JTable jTable, Object o, boolean b, int i, int i1) {
        textField = new JTextField();
        textField.setText(value);
        textField.addKeyListener(this);
        return textField;
    }

    @Override
    public String getCellEditorValue() {
        if (textField == null) {
            return value;
        } else {
            return textField.getText();
        }
    }

    @Override
    public void keyTyped(KeyEvent keyEvent) {
    }

    @Override
    public void keyPressed(KeyEvent keyEvent) {
    }

    @Override
    public void keyReleased(KeyEvent keyEvent) {
        //noinspection unchecked
        this.colInfo.setValue(elementData, textField.getText());
    }
}
