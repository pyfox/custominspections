package com.pyfips.intellij.custominspections;

import com.intellij.openapi.components.PersistentStateComponent;
import com.intellij.openapi.components.State;
import com.intellij.openapi.components.Storage;
import com.intellij.openapi.ui.Messages;
import org.jdom.Element;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

@State(
        name = "CustomInspectionsConfig",
        storages = {
                @Storage("CustomInspectionsConfig.xml")}
)
class CustomInspectionsConfig implements PersistentStateComponent<Element> {
    static class CompiledState {
        public Map<String, List<ElementData>> state = new HashMap<>();
    }

    static class InternalState {
        public Map<String, List<ElementData>> state = new HashMap<>();
    }

    InternalState myState;  // State as it is saved in the config file.
    CompiledState compiledState;  // State with precompiled regex.

    /**
     * Returns the content patterns that have to be applied to a specific file.
     */
    public List<ElementData> getPatterns(String fileName) {
        List<ElementData> results = new ArrayList<>();
        for (Map.Entry<String, List<ElementData>> dictEntry : compiledState.state.entrySet()) {
            if (fileName.matches(dictEntry.getKey())) {
                results.addAll(dictEntry.getValue());
            }
        }
        return results;
    }

    public void setValue(String filePattern, String newValue, String newReplacement, String newComment, String newSeverity) {
        if (!myState.state.containsKey(filePattern)) {
            List<ElementData> emptyList = new ArrayList<>();
            myState.state.put(filePattern, emptyList);
        }

        try {
            Pattern.compile(filePattern);  // Compile filepattern to test if it is valid.
            myState.state.get(filePattern).add(new ElementData(filePattern, newValue, newReplacement, newComment, newSeverity));
        } catch (Exception e) {
            Messages.showDialog(e.toString(), "Invalid Pattern", new String[]{"Ok"}, 0,
                    Messages.getErrorIcon());
            throw e;  // Explicitly throw exception, otherwise the settings dialog will close.
        }
    }

    public void clearState() {
        myState = new InternalState();
        updateCompiledState();
    }

    /**
     * Returns the xml element for saving the current configuration.
     */
    public Element getState() {
        final Element config = new Element("config");

        for (Map.Entry<String, List<ElementData>> dictEntry : myState.state.entrySet()) {
            Element extEntry = new Element("filetypeContainer");
            extEntry.setAttribute("regex", dictEntry.getKey());
            for (int i = 0; i < dictEntry.getValue().size(); i++) {
                ElementData listEntry = dictEntry.getValue().get(i);
                Element element = new Element("element");
                element.setAttribute("id", String.valueOf(i));
                Element comment = new Element("comment");
                comment.setText(listEntry.getDescription());
                element.addContent(comment);
                Element regex = new Element("regex");
                regex.setText(listEntry.getContentPattern());
                element.addContent(regex);
                Element replacement = new Element("replacement");
                replacement.setText(listEntry.getReplacementPattern());
                element.addContent(replacement);
                Element severity = new Element("severity");
                severity.setText(listEntry.getSeverity());
                element.addContent(severity);
                extEntry.addContent(element);
            }
            config.addContent(extEntry);
        }
        return config;
    }

    /**
     * Loads a saved state.
     */
    public void loadState(Element state) {
        clearState();
        List<Element> extEntries = state.getChildren();
        for (Element extEntry : extEntries) {
            List<Element> listEntries = extEntry.getChildren();
            for (Element listEntry : listEntries) {
                String severity = listEntry.getChild("severity").getText();
                if (severity == null) {
                    severity = "Warning";
                }
                Element replacementNode = listEntry.getChild("replacement");
                String replacement = "";
                if (replacementNode != null) {
                    replacement = replacementNode.getText();
                }
                setValue(extEntry.getAttributeValue("regex"),
                        listEntry.getChild("regex").getText(),
                        replacement,
                        listEntry.getChild("comment").getText(),
                        severity);
            }
        }
        updateCompiledState();
    }

    /**
     * Does a precompiling of certain regex expressions and saves them in an optimized state for
     * quick access.
     */
    public void updateCompiledState() {
        compiledState = new CompiledState();
        compiledState.state.putAll(myState.state);
    }

    /**
     * Called, when previous state is available, e.g. if the plugin has just been installed.
     */
    @Override
    public void noStateLoaded() {
        myState = new InternalState();
        List<ElementData> txtExamples = new ArrayList<>();
        txtExamples.add(new ElementData(".*\\.txt", "Forbidden (\\w+)", "Allowed $1", "Just do not write this.", "Warning"));
        myState.state.put(".*\\.txt", txtExamples);
        updateCompiledState();
    }
}